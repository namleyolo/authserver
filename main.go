package main

import (
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"github.com/micro/go-micro/client"
	"github.com/micro/go-micro/transport"
	"github.com/micro/go-micro/util/log"
	"github.com/micro/go-micro/web"
	"github.com/tkanos/gonfig"
	"gitlab.com/vitconduck/authserver/models"
	"gitlab.com/vitconduck/authserver/redis"
	"golang.org/x/crypto/bcrypt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"net/smtp"
	"os"
	"strconv"
	"strings"
	"time"
)

type smtpServer struct {
	host string
	port string
}

func (s *smtpServer) Address() string {
	return s.host + ":" + s.port
}

func init() {
	_ = client.DefaultClient.Init(
		client.Transport(
			transport.NewTransport(transport.Secure(true)),
		),
	)

}

func main() {
	var arg string
	if len(os.Args) > 1 {
		arg = os.Args[1]
	}
	local := arg == "local"
	log.Info("local:==", local)

	redisConnect := redis.NewClient(local)
	defer redisConnect.Close()
	err := godotenv.Load() //Load .env file
	type Token struct {
		Username string `json:"username"`
		jwt.StandardClaims
	}
	//var token_password  thisIsTheJwtSecretPassword
	type User struct {
		Id       string `json:"id"`
		Username string `json:"username"`
		Password string `json:"password"`
		Phone    string `json:"phone"`
		AppleId  string `json:"apple_id"`
		//Status string  `json:"token";sql:"-"`
		Status       *int64    `json:"status"`
		Permission   int64     `json:"permission"`
		GoogleName   *string   `json:"google_name"`
		RefreshToken string    `json:"refresh_token"`
		Email        string    `json:"email"`
		AvatarGoogle string    `json:"avatar_google"`
		DisplayName  string    `json:"display_name"`
		LastLogined  time.Time `json:"last_logined"`
		IsActivated  bool      `json:"is_activated"`
	}

	type GoogleTokenId struct {
		Token string `json:"token"`
	}

	type NewPassword struct {
		Email      string `json:"email"`
		Password   string `json:"password"`
		VerifyCode string `json:"verify_code"`
	}
	type ChangePassword struct {
		Email       string `json:"email"`
		OldPassword string `json:"old_password"`
		NewPassword string `json:"new_password"`
	}

	type VerifyCode struct {
		Email      string `json:"email"`
		VerifyCode string `json:"verify_code"`
	}
	var configuration Configuration
	if local {
		log.Info("local Configuration")
		configuration = GetConfig()
	} else {
		log.Info("product Configuration")
		configuration = GetConfig("prod")
	}
	log.Info(configuration.DB_USERNAME)
	log.Info(configuration.DB_PASSWORD)
	log.Info(configuration.DB_PORT)
	log.Info(configuration.DB_NAME)
	db, err := gorm.Open("postgres",
		"user="+configuration.DB_USERNAME+" dbname="+configuration.DB_NAME+" password="+configuration.DB_PASSWORD+" port="+configuration.DB_PORT+" host="+configuration.DB_HOST+" sslmode=disable")
	fmt.Println("user=" + configuration.DB_USERNAME + " dbname=" + configuration.DB_NAME + " password=" + configuration.DB_PASSWORD + " port=" + configuration.DB_PORT + " host=" + configuration.DB_HOST + " sslmode=disable")
	//db, err := gorm.Open("postgres", "user=postgres dbname=newest password=postgres port=5432 host=localhost sslmode=disable")

	if err != nil {
		panic(err)
	}
	defer db.Close()

	database := db.DB()
	err = database.Ping()
	if err != nil {
		panic(err)
	}
	log.Info("connected to DB")

	r := gin.Default()
	service := web.NewService(
		web.Name("go.micro.api.auth"),
		web.Version("1.17"),
		web.Address(":9096"),
		web.Handler(r),
	)
	r.Use(CORSMiddleware())

	if err := service.Init(); err != nil {
		panic(err)
	}
	r.POST("/isLogin", func(c *gin.Context) {
		log.Info("From %s%s\n", c.Request.Host, c.Request.URL.Path)

		tokenString := c.GetHeader("Access-Token")
		tk := &Token{}
		verifyToken, err := jwt.ParseWithClaims(tokenString, tk, func(token *jwt.Token) (interface{}, error) {
			return []byte(os.Getenv("jwtAccessKey")), nil
		})

		if err != nil {
			c.Status(402)
			c.Header("Access-Permission", "denied")
		}
		if verifyToken != nil && verifyToken.Valid == true {
			c.Header("Access-Permission", "accept")
		} else {
			c.Status(http.StatusForbidden)
		}
	})
	service.HandleFunc("/test", func(writer http.ResponseWriter, request *http.Request) {
		switch request.Method {
		case "POST":
			auth := smtp.PlainAuth("", "dicomiotservice@gmail.com", "uuijiwcohupaskza", "smtp.gmail.com")

			// Here we do it all: connect to our server, set up a message and send it
			to := []string{"happybear02042000@gmail.com"}
			msg := []byte("To: happybear02042000@gmail.com\r\n" +
				"Subject: Why are you not using Mailtrap yet?\r\n" +
				"\r\n" +
				"Here’s the space for our great sales pitch\r\n")
			err := smtp.SendMail("smtp.gmail.com:587", auth, "dicomiotservice@gmail.com", to, msg)
			if err != nil {
				log.Fatal(err)
			}
		}
	})

	r.POST("/forgetPassword", func(c *gin.Context) {
		log.Info(c.ClientIP())
		rand.Seed(time.Now().UnixNano())
		min := 100000
		max := 999999
		randomCode := rand.Intn(max-min+231) + min
		var user User
		body, err := ioutil.ReadAll(c.Request.Body)
		if err != nil {
			log.Info(err.Error())
		}

		err = json.Unmarshal(body, &user)
		getUser := db.Debug().First(&user, "email = ?", user.Email)
		if getUser.RowsAffected != 0 {
			c.Header("ResponseCode", strconv.Itoa(randomCode))
			b, err := json.MarshalIndent(getUser.Value, "", "")
			err = json.Unmarshal(b, &user)
			log.Info(user)
			//redisConnect := redis.NewClient()
			ping, err := redisConnect.Ping().Result()
			if err != nil {
				log.Fatal(err)
				//w.Header().Set("ResponseCode", "3")
			} else {
				log.Info(ping)
				convertRandomCodeToString := strconv.Itoa(randomCode)

				go func() {
					err = redisConnect.Set(user.Email,
						convertRandomCodeToString,
						time.Duration(15)*time.Minute).Err()
				}()
				go func() {
					from := os.Getenv("gmail")
					pass := "uuijiwcohupaskza"
					to := user.Email
					bodyMessage := "Xin chào " + user.Username + " \n" + "Bạn đã yêu cầu thiết lập lại mật khẩu của mình trên ứng dụng DICOM \n" +
						"Mã xác nhận của bạn là : " + strconv.Itoa(randomCode) + "\n" +
						"Mã xác thực có hiệu lực trong 15 phút"
					msg := "From: " + from + "\n" +
						"To: " + to + "\n" +
						"Subject: Forget password\n\n" + bodyMessage
					err = smtp.SendMail("smtp.gmail.com:587",
						smtp.PlainAuth("", from, pass, "smtp.gmail.com"),
						from, []string{to}, []byte(msg))
				}()
			}
		} else {
			c.JSON(400, "Not found the email")
		}
	})
	r.POST("/newPassword", func(c *gin.Context) {
		var newPw NewPassword
		body, _ := ioutil.ReadAll(c.Request.Body)
		var user User
		_ = json.Unmarshal(body, &newPw)
		if len(newPw.Email) < 1 {
			return
		}
		getVerifyCode := newPw.VerifyCode
		//redisConnect := redis.NewClient()
		//defer redis.NewClient().Close()
		getCodeRedis := redisConnect.Get(newPw.Email)
		if len(getCodeRedis.Val()) == 0 {
			c.Header("ResponseCode", CODE_INVALID_INPUT)
		}
		if getCodeRedis.Val() == getVerifyCode {
			hashPw, _ := HashPassword(newPw.Password)
			user.Email = newPw.Email
			err := db.Debug().Model(&user).Where("email =?", newPw.Email).Update("password", hashPw)
			if err != nil {
				log.Info(err)
			}
			db.LogMode(true)
			c.Header("ResponseCode", "0")
		} else {
			c.Header("ResponseCode", "1")
		}
	})
	r.POST("/changePassword", func(c *gin.Context) {
		var changePassword ChangePassword
		body, _ := ioutil.ReadAll(c.Request.Body)
		var user User
		err = json.Unmarshal(body, &changePassword)
		if len(changePassword.Email) < 1 {
			return
		}
		//oldPassword := changePassword.OldPassword
		//redisConnect := redis.NewClient()
		//defer redis.NewClient().Close()
		//getCodeRedis := redisConnect.Get(changePassword.Email)
		//hashOldPw, _ := HashPassword(changePassword.OldPassword)
		//log.Debug(hashOldPw)
		//isExistUserEmail := db.Debug().Where("email =?", user.Email).First(&user)
		isExistUserEmail := db.Debug().Where("email=?", changePassword.Email).First(&user)
		pw := changePassword.OldPassword
		if isExistUserEmail.RowsAffected == 0 {
			c.Writer.WriteHeader(http.StatusBadRequest)
			c.Header("ResponseCode", CODE_INVALID_INPUT)
			return
		}
		match := ComparePassword(pw, user.Password)

		if !match {
			c.Writer.WriteHeader(http.StatusBadRequest)
			c.Header("ResponseCode", CODE_INVALID_INPUT)
			return
		}
		newPw, err := HashPassword(changePassword.NewPassword)
		err2 := db.Debug().Model(&user).Where("email =?", changePassword.Email).Update("password", newPw)

		if err2.Error != nil {
			log.Info(err)
			c.Writer.WriteHeader(http.StatusInternalServerError)
			c.Header("ResponseCode", CODE_INVALID_INPUT)
		} else {
			db.LogMode(true)
			c.Writer.WriteHeader(http.StatusAccepted)
			c.Header("ResponseCode", CODE_SUCCESS)
		}

	})
	r.POST("/verifyCode", func(c *gin.Context) {
		var verifycode VerifyCode
		body, err := ioutil.ReadAll(c.Request.Body)
		_ = json.Unmarshal(body, &verifycode)
		getVerifyCode := verifycode.VerifyCode
		//redisConnect := redis.NewClient()
		//defer redis.NewClient().Close()
		getCodeRedis := redisConnect.Get(verifycode.Email)
		if len(getCodeRedis.Val()) == 0 {
			c.Header("ResponseCode", "2")
		}
		if getCodeRedis.Val() == getVerifyCode {
			/*hashPw, _ := HashPassword(newPw.Password)
			user.Email = newPw.Email
			err := db.Debug().Model(&user).Where("email =?", newPw.Email).Update("password", hashPw)
			*/if err != nil {
				log.Info(err)
			}
			db.LogMode(true)
			c.Header("ResponseCode", CODE_SUCCESS)
		} else {
			c.Header("ResponseCode", CODE_INVALID_INPUT)
		}

	})
	r.POST("/registerUser", func(c *gin.Context) {
		var user User
		body, err := ioutil.ReadAll(c.Request.Body)

		err = json.Unmarshal(body, &user)
		if len(user.Email) < 1 || len(user.Password) < 1 {
			return
		}
		isExistUserEmail := db.Where("email=?", user.Email).First(&user)
		if isExistUserEmail.RowsAffected != 0 {
			c.Header("ResponseCode", CODE_USER_EXISTED)
			return
		} else {
			uuidRandom := uuid.New().String()
			hashPw, _ := HashPassword(user.Password)
			user.Password = hashPw

			newUser := User{
				Id:           uuidRandom,
				Username:     user.Email,
				Password:     hashPw,
				RefreshToken: "",
				Email:        user.Email,
				AvatarGoogle: "",
				DisplayName:  "",
				LastLogined:  time.Now(),
				IsActivated:  false,
			}
			resultDB := db.Debug().Create(&newUser)

			if resultDB.Error != nil {
				log.Info(resultDB.Error.Error())
				return
			} else {
				//add send verify code
				rand.Seed(time.Now().UnixNano())
				min := 100000
				max := 999999
				randomCode := rand.Intn(max-min) + min
				convertToString := strconv.Itoa(randomCode)

				go func() {
					//defer redisConnect.Close()
					err = redisConnect.Set(user.Email,
						convertToString,
						time.Duration(15)*time.Minute).Err()
				}()

				go func() {
					sendVerificationEmail(user.Username, user.Email, randomCode, db, true)
				}()
				c.Header("ResponseCode", CODE_SUCCESS)
				c.Header("UserId", uuidRandom)
			}
		}
	})
	r.POST("/registerPhone", func(c *gin.Context) {
		var user User
		body, err := ioutil.ReadAll(c.Request.Body)

		err = json.Unmarshal(body, &user)
		if len(user.Email) < 1 || len(user.Password) < 1 {
			return
		}
		isExistUserEmail := db.Where("username=?", user.Phone).First(&user)
		if isExistUserEmail.RowsAffected != 0 {
			c.Header("ResponseCode", CODE_USER_EXISTED)
			return
		} else {
			uuidRandom := uuid.New().String()
			hashPw, _ := HashPassword(user.Password)
			user.Password = hashPw

			newUser := User{
				Id:           uuidRandom,
				Username:     user.Email,
				Password:     hashPw,
				RefreshToken: "",
				Email:        user.Email,
				AvatarGoogle: "",
				DisplayName:  "",
				LastLogined:  time.Now(),
				IsActivated:  false,
			}
			resultDB := db.Debug().Create(&newUser)

			if resultDB.Error != nil {
				log.Info(resultDB.Error.Error())
				return
			} else {
				//add send verify code
				rand.Seed(time.Now().UnixNano())
				min := 100000
				max := 999999
				randomCode := rand.Intn(max-min) + min
				convertToString := strconv.Itoa(randomCode)

				go func() {
					//defer redisConnect.Close()
					err = redisConnect.Set(user.Email,
						convertToString,
						time.Duration(15)*time.Minute).Err()
				}()

				go func() {
					sendVerificationEmail(user.Username, user.Email, randomCode, db, true)
				}()
				c.Header("ResponseCode", CODE_SUCCESS)
				c.Header("UserId", uuidRandom)
			}
		}
	})

	r.POST("/registerUserFull", func(c *gin.Context) {
		log.Info("test")
		var user User
		body, err := ioutil.ReadAll(c.Request.Body)

		err = json.Unmarshal(body, &user)
		if len(user.Email) < 1 || len(user.Password) < 1 {
			c.Writer.WriteHeader(http.StatusBadRequest)
			c.Header("ResponseCode", CODE_INVALID_INPUT)

		} else {
			isExistUserEmail := db.Where("email=?", user.Email).First(&user)
			if isExistUserEmail.RowsAffected != 0 {

				log.Info("user existed")
				c.Writer.WriteHeader(http.StatusBadRequest)
				c.Header("ResponseCode", CODE_USER_EXISTED)
				//return
			} else {
				log.Info("making new user")
				uuidRandom := uuid.New().String()
				hashPw, _ := HashPassword(user.Password)
				user.Password = hashPw
				//new user is not activated
				newUser := User{
					Id:           uuidRandom,
					Username:     user.Email,
					Password:     hashPw,
					Phone:        "",
					RefreshToken: "",
					Email:        user.Email,
					AvatarGoogle: "",
					DisplayName:  user.DisplayName,
					LastLogined:  time.Now(),
					IsActivated:  false,
				}

				resultDB := db.Debug().Create(&newUser)

				if resultDB.Error != nil {
					log.Info(resultDB.Error.Error())
					return
				} else {
					//add verify code

					rand.Seed(time.Now().UnixNano())
					min := 100000
					max := 999999
					randomCode := rand.Intn(max-min) + min
					convertToString := strconv.Itoa(randomCode)

					go func() {
						//defer redisConnect.Close()
						err = redisConnect.Set(user.Email,
							convertToString,
							time.Duration(15)*time.Minute).Err()
					}()

					go func() {
						sendVerificationEmail(user.DisplayName, user.Email, randomCode, db, true)
					}()
					c.Writer.WriteHeader(http.StatusAccepted)
					c.Header("ResponseCode", "0")
					c.Header("UserId", uuidRandom)
				}
			}
		}
	})

	r.POST("/sendVerifycode", func(c *gin.Context) {
		var user User
		body, err := ioutil.ReadAll(c.Request.Body)

		err = json.Unmarshal(body, &user)
		if len(user.Email) < 1 {
			c.Writer.WriteHeader(http.StatusBadRequest)
			c.Header("ResponseCode", CODE_INVALID_INPUT)
		}
		isExistUserEmail := db.Where("email=?", user.Email).First(&user)
		if isExistUserEmail.RowsAffected == 0 {
			c.Writer.WriteHeader(http.StatusBadRequest)
			c.Header("ResponseCode", CODE_USER_EXISTED)
			return
		} else {

			//add verify code

			rand.Seed(time.Now().UnixNano())
			min := 100000
			max := 999999
			randomCode := rand.Intn(max-min) + min
			convertToString := strconv.Itoa(randomCode)

			go func() {
				//defer redisConnect.Close()
				err = redisConnect.Set(user.Email,
					convertToString,
					time.Duration(15)*time.Minute).Err()
			}()

			go func() {
				sendVerificationEmail(user.DisplayName, user.Email, randomCode, db, true)
			}()
			c.Writer.WriteHeader(http.StatusAccepted)
			c.Header("ResponseCode", CODE_SUCCESS)
		}

	})

	r.POST("/activeUser", func(c *gin.Context) {
		var request VerifyCode
		body, _ := ioutil.ReadAll(c.Request.Body)
		_ = json.Unmarshal(body, &request)

		//getCodeRedis := redisConnect.Get(user)
		getCodeRedis := redisConnect.Get(request.Email)
		if len(getCodeRedis.Val()) == 0 {
			c.Header("ResponseCode", "2")
		}

		var user User
		if getCodeRedis.Val() == request.VerifyCode {

			err := db.Debug().Model(&user).Where("email =?", request.Email).Update("isActivated", true)
			if err != nil {
				log.Info(err)
			}
			db.LogMode(true)
			c.Header("ResponseCode", CODE_SUCCESS)
		} else {
			c.Header("ResponseCode", CODE_INVALID_INPUT)
		}
	})
	r.POST("/checkNumber", func(c *gin.Context) {
		number := c.GetHeader("phone")

		var user User
		//if getCodeRedis.Val() == request.VerifyCode {
		result := db.Where("username=?", number).First(&user)
		if result.Error != nil {
			log.Error(result.Error)
		}
		if result.RowsAffected == 0 {
			log.Info("phone number not existed ")
			c.Writer.WriteHeader(http.StatusBadRequest)
			c.Header("ResponseCode", CODE_INVALID_INPUT)
		} else {
			c.Writer.WriteHeader(http.StatusOK)
			c.Header("ResponseCode", CODE_SUCCESS)
		}
	})

	r.POST("/refreshToken", func(c *gin.Context) {
		tokenString := c.GetHeader("Refresh-Token")
		tk := &Token{}
		verifyToken, err := jwt.ParseWithClaims(tokenString, tk, func(token *jwt.Token) (interface{}, error) {
			return []byte(os.Getenv("jwtRefreshKey")), nil
		})
		if err != nil {
			c.JSON(400, gin.H{"message": "bad request"})
		}
		if verifyToken != nil && verifyToken.Valid == true {
			c.Header("Access-Permissions", "accept")
		} else {
			c.Writer.WriteHeader(http.StatusUnauthorized)
		}

		var user User
		getUser := db.Debug().Where("refresh_token = ?", tokenString).First(&user)
		b, _ := json.Marshal(getUser.Value)
		err = json.Unmarshal(b, &user)
		if err != nil || getUser.RowsAffected != 1 {
			c.Header("Access-Permission", "denied")
			c.Writer.WriteHeader(http.StatusForbidden)
		} else {
			expirationAccessTokenTime := time.Now().Add(60 * time.Hour) // create expire time : 2 hours
			accessTk := &Token{
				Username:       user.Username,
				StandardClaims: jwt.StandardClaims{ExpiresAt: expirationAccessTokenTime.Unix()},
			}
			db.LogMode(true)
			tokenAcc := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), accessTk)
			accessTokenString, _ := tokenAcc.SignedString([]byte(os.Getenv("jwtAccessKey")))
			c.Writer.WriteHeader(http.StatusAccepted)
			c.Header("Access-Token", accessTokenString)
			c.Header("Access-Permission", "accept")
		}
	})
	r.POST("/login", func(c *gin.Context) {
		var user User
		body, err := ioutil.ReadAll(c.Request.Body)
		if err != nil {
			log.Info(err.Error())
			return
		}
		getTokenDevice := c.GetHeader("token_devices")
		log.Info("getTokenDevice :::", getTokenDevice)

		err = json.Unmarshal(body, &user)

		inputPw := user.Password

		res := db.Where("email =? or username =? ", user.Email, user.Email).First(&user)

		if res.RowsAffected == 0 {
			log.Info("can not get user ")
			c.Writer.WriteHeader(http.StatusBadRequest)
			c.Header("ResponseCode", CODE_INVALID_INPUT)
		} else {

			b, _ := json.Marshal(res.Value)
			err = json.Unmarshal(b, &user)
			match := ComparePassword(inputPw, user.Password)
			// return 1 if user is not activated
			if !user.IsActivated {
				c.Writer.WriteHeader(http.StatusForbidden)
				c.Header("ResponseCode", string(CODE_NOT_ACTIVATED))
			} else if match == true {
				log.Info("match password user")
				expirationAccessTime := time.Now().Add(72 * time.Hour) // create expire time
				expirationRefreshTime := time.Now().Add(1180 * time.Hour)
				accessTk := &Token{
					Username:       user.Email,
					StandardClaims: jwt.StandardClaims{ExpiresAt: expirationAccessTime.Unix()},
				}
				refreshTk := &Token{
					Username:       user.Email,
					StandardClaims: jwt.StandardClaims{ExpiresAt: expirationRefreshTime.Unix()},
				}
				tokenAcc := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), accessTk)
				tokenRefresh := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), refreshTk)

				accessTokenString, _ := tokenAcc.SignedString([]byte(os.Getenv("jwtAccessKey")))
				refreshTokenString, _ := tokenRefresh.SignedString([]byte(os.Getenv("jwtRefreshKey")))

				c.Header("Access-Token", accessTokenString)
				c.Header("Refresh-Token", refreshTokenString)

				user.RefreshToken = refreshTokenString //Store the token in the response
				user.LastLogined = time.Now()
				c.Header("UserId", user.Id)

				userDeviceToken := models.UserDeviceToken{
					UserId:      user.Id,
					DeviceToken: getTokenDevice,
				}

				timeNow := time.Now()
				db.Debug().Save(&user)
				db.Debug().Where(userDeviceToken).FirstOrCreate(&userDeviceToken)

				log.Info("finished with time : %f", time.Since(timeNow).Seconds())
			} else {
				c.Writer.WriteHeader(http.StatusForbidden)
				c.Header("ResponseCode", CODE_INVALID_INPUT)

			}

			//db.LogMode(true)

		}
	})
	r.POST("/checkAuth", func(c *gin.Context) {
		tokenString := c.GetHeader("Access-Token")
		if len(tokenString) == 0 {
			c.JSON(400, "Bad request")
		}
		tk := &Token{}

		verifyToken, err := jwt.ParseWithClaims(tokenString, tk, func(token *jwt.Token) (interface{}, error) {
			return []byte(os.Getenv("jwtAccessKey")), nil
		})
		if err != nil {
			if err == jwt.ErrSignatureInvalid {
				log.Info("err invalid signature")
				c.Status(403)
				c.Writer.WriteHeader(http.StatusUnauthorized)
				return
			}

			if strings.Contains(err.Error(), "expired") {
				c.Header("expired", "expired")
			}
			c.Writer.WriteHeader(http.StatusUnauthorized)
			c.Header("Access-Permission", "denied")
		} else {
			c.Header("Access-Permission", "accept")
		}
		if !verifyToken.Valid {
			c.Header("Access-Permission", "denied")
			//w.WriteHeader(http.StatusUnauthorized)
			return
		}
	})

	r.POST("/loginGoogleMobile", func(c *gin.Context) {
		var token GoogleTokenId
		body, err := ioutil.ReadAll(c.Request.Body)
		if err != nil {
			return
		}
		err = json.Unmarshal(body, &token)

		url := "https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=" + token.Token
		res, err := http.Get(url)
		if err != nil {
			log.Fatal(err)
		}
		type GoogleInfo struct {
			Email   string `json:"email"`
			Sub     string `json:"sub"`
			Exp     string `json:"exp"`
			Name    string `json:"name"`
			Picture string `json:"picture"`
		}
		var ggInfo GoogleInfo
		var tm time.Time
		tokenDevice := c.GetHeader("device-token")

		robots, err := ioutil.ReadAll(res.Body)
		_ = json.Unmarshal(robots, &ggInfo)
		v, _ := strconv.ParseInt(ggInfo.Exp, 10, 64)
		tm = time.Unix(v, 0)
		_ = tm
		//log.Info(time.Now().Unix())

		if len(ggInfo.Email) > 0 {
			var user User
			getUser := db.Debug().Where("email = ?", ggInfo.Email).First(&user)
			expirationAccessTime := time.Now().Add(60 * time.Hour) // create expire time
			expirationRefreshTime := time.Now().Add(1180 * time.Hour)
			accessTk := &Token{
				Username:       ggInfo.Email,
				StandardClaims: jwt.StandardClaims{ExpiresAt: expirationAccessTime.Unix()},
			}
			newRefreshTk := &Token{
				Username:       ggInfo.Email,
				StandardClaims: jwt.StandardClaims{ExpiresAt: expirationRefreshTime.Unix()},
			}
			tokenAcc := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), accessTk)
			tokenRefresh := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), newRefreshTk)

			accessTokenString, _ := tokenAcc.SignedString([]byte(os.Getenv("jwtAccessKey")))
			refreshTokenString, _ := tokenRefresh.SignedString([]byte(os.Getenv("jwtRefreshKey")))
			if getUser.RowsAffected != 0 {
				db.Model(&user).Debug().Updates(map[string]interface{}{"refresh_token": refreshTokenString, "last_logined": time.Now()})
				var usName User
				b, _ := json.Marshal(getUser.Value)
				err = json.Unmarshal(b, &usName)
				c.Header("User-Id", usName.Id)
				userDeviceToken := models.UserDeviceToken{
					UserId:      usName.Id,
					DeviceToken: tokenDevice,
				}
				db.Debug().Where(userDeviceToken).FirstOrCreate(&userDeviceToken)
				db.LogMode(true)

			} else {
				uuidRandom := uuid.New().String()

				c.Header("User-Id", uuidRandom)
				hashedPw, _ := HashPassword("")
				//google user activated = true
				newUser := User{
					Id:           uuidRandom,
					Username:     ggInfo.Email,
					Password:     hashedPw,
					RefreshToken: refreshTokenString,
					Email:        ggInfo.Email,
					AvatarGoogle: ggInfo.Picture,
					DisplayName:  ggInfo.Name,
					LastLogined:  time.Now(),
					IsActivated:  true,
				}
				addUser := db.Debug().Save(&newUser)
				userDeviceToken := models.UserDeviceToken{
					UserId:      uuidRandom,
					DeviceToken: tokenDevice,
				}
				db.Debug().Where(userDeviceToken).FirstOrCreate(&userDeviceToken)

				if addUser.Error != nil {
					log.Info("err::", "can not create")
					c.Writer.WriteHeader(http.StatusInternalServerError)
				}

			}

			db.LogMode(true)
			c.Header("Access-Token", accessTokenString)
			c.Header("Refresh-Token", refreshTokenString)
		}

		err = res.Body.Close()
		if err != nil {
			return
		}
	})

	r.POST("/loginAppleMobile", func(c *gin.Context) {

		type LoginAppleMsg struct {
			Name  string `json:"name"`
			Id    string `json:"id"`
			Token string `json:"token"`
		}
		body, err := ioutil.ReadAll(c.Request.Body)
		if err != nil {
			return
		}
		/*var token GoogleTokenId
		err = json.Unmarshal(body, &token)
		*/
		var tokenMsg LoginAppleMsg
		err = json.Unmarshal(body, &tokenMsg)

		/*url := "https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=" + token.Token
		res, err := http.Get(url)
		if err != nil {
			log.Fatal(err)
		}
		type GoogleInfo struct {
			Email   string `json:"email"`
			Sub     string `json:"sub"`
			Exp     string `json:"exp"`
			Name    string `json:"name"`
			Picture string `json:"picture"`
		}
		var ggInfo GoogleInfo
		var tm time.Time
		tokenDevice := c.GetHeader("device-token")

		robots, err := ioutil.ReadAll(res.Body)
		_ = json.Unmarshal(robots, &ggInfo)
		v, _ := strconv.ParseInt(ggInfo.Exp, 10, 64)
		tm = time.Unix(v, 0)
		_ = tm
		*/ //log.Info(time.Now().Unix())
		//if ()
		var user User
		rs := db.Debug().Where("email = ?", tokenMsg.Id).First(&user)
		if rs.Error != nil {
			log.Error(rs.Error.Error())
			return
		}
		tokenDevice := c.GetHeader("device-token")

		expirationAccessTime := time.Now().Add(60 * time.Hour) // create expire time
		expirationRefreshTime := time.Now().Add(1180 * time.Hour)
		accessTk := &Token{
			Username:       tokenMsg.Id,
			StandardClaims: jwt.StandardClaims{ExpiresAt: expirationAccessTime.Unix()},
		}
		newRefreshTk := &Token{
			Username:       tokenMsg.Id,
			StandardClaims: jwt.StandardClaims{ExpiresAt: expirationRefreshTime.Unix()},
		}
		tokenAcc := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), accessTk)
		tokenRefresh := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), newRefreshTk)

		accessTokenString, _ := tokenAcc.SignedString([]byte(os.Getenv("jwtAccessKey")))
		refreshTokenString, _ := tokenRefresh.SignedString([]byte(os.Getenv("jwtRefreshKey")))

		if rs.RowsAffected != 0 {

			db.Model(&user).Debug().Updates(map[string]interface{}{"refresh_token": refreshTokenString, "last_logined": time.Now()})
			var usName User
			b, _ := json.Marshal(rs.Value)
			err = json.Unmarshal(b, &usName)
			c.Header("User-Id", usName.Id)
			userDeviceToken := models.UserDeviceToken{
				UserId:      usName.Id,
				DeviceToken: tokenDevice,
			}
			db.Debug().Where(userDeviceToken).FirstOrCreate(&userDeviceToken)
			db.LogMode(true)

		} else {
			uuidRandom := uuid.New().String()

			c.Header("User-Id", uuidRandom)
			hashedPw, _ := HashPassword("")
			//google user activated = true
			newUser := User{
				Id:      uuidRandom,
				AppleId: tokenMsg.Id,
				//Username:     tokenMsg.Id,
				Password:     hashedPw,
				RefreshToken: refreshTokenString,
				Email:        tokenMsg.Id,
				//AvatarGoogle: ggInfo.Picture,
				DisplayName: tokenMsg.Name,
				LastLogined: time.Now(),
				IsActivated: true,
			}
			addUser := db.Debug().Save(&newUser)
			userDeviceToken := models.UserDeviceToken{
				UserId:      uuidRandom,
				DeviceToken: tokenDevice,
			}
			db.Debug().Where(userDeviceToken).FirstOrCreate(&userDeviceToken)

			if addUser.Error != nil {
				log.Info("err::", "can not create")
				c.Writer.WriteHeader(http.StatusInternalServerError)
			}
		}
		db.LogMode(true)
		c.Header("Access-Token", accessTokenString)
		c.Header("Refresh-Token", refreshTokenString)

		/*if len(ggInfo.Email) > 0 {
			var user User
			getUser := db.Debug().Where("email = ?", ggInfo.Email).First(&user)
			expirationAccessTime := time.Now().Add(60 * time.Hour) // create expire time
			expirationRefreshTime := time.Now().Add(1180 * time.Hour)
			accessTk := &Token{
				Username:       ggInfo.Email,
				StandardClaims: jwt.StandardClaims{ExpiresAt: expirationAccessTime.Unix()},
			}
			newRefreshTk := &Token{
				Username:       ggInfo.Email,
				StandardClaims: jwt.StandardClaims{ExpiresAt: expirationRefreshTime.Unix()},
			}
			tokenAcc := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), accessTk)
			tokenRefresh := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), newRefreshTk)

			accessTokenString, _ := tokenAcc.SignedString([]byte(os.Getenv("jwtAccessKey")))
			refreshTokenString, _ := tokenRefresh.SignedString([]byte(os.Getenv("jwtRefreshKey")))
			if getUser.RowsAffected != 0 {
				db.Model(&user).Debug().Updates(map[string]interface{}{"refresh_token": refreshTokenString, "last_logined": time.Now()})
				var usName User
				b, _ := json.Marshal(getUser.Value)
				err = json.Unmarshal(b, &usName)
				c.Header("User-Id", usName.Id)
				userDeviceToken := models.UserDeviceToken{
					UserId:      usName.Id,
					DeviceToken: tokenDevice,
				}
				db.Debug().Where(userDeviceToken).FirstOrCreate(&userDeviceToken)
				db.LogMode(true)

			} else {


			}

		}
		*/
		//err = res.Body.Close()
		//if err != nil {
		//	return
		//}
	})

	r.POST("/loginMobileFirebase", func(c *gin.Context) {
		var token GoogleTokenId
		body, err := ioutil.ReadAll(c.Request.Body)
		if err != nil {
			return
		}
		err = json.Unmarshal(body, &token)

		url := "https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=" + token.Token
		res, err := http.Get(url)
		if err != nil {
			log.Fatal(err)
		}
		type GoogleInfo struct {
			Email       string `json:"email"`
			Sub         string `json:"sub"`
			Exp         string `json:"exp"`
			Name        string `json:"name"`
			Picture     string `json:"picture"`
			PhoneNumber string `json:"phone_number"`
		}
		var ggInfo GoogleInfo
		var tm time.Time
		tokenDevice := c.GetHeader("device-token")

		robots, err := ioutil.ReadAll(res.Body)
		_ = json.Unmarshal(robots, &ggInfo)
		v, _ := strconv.ParseInt(ggInfo.Exp, 10, 64)
		tm = time.Unix(v, 0)
		_ = tm
		//log.Info(time.Now().Unix())

		if len(ggInfo.PhoneNumber) > 0 {
			var user User
			getUser := db.Debug().Where("email = ?", ggInfo.Email).First(&user)
			expirationAccessTime := time.Now().Add(60 * time.Hour) // create expire time
			expirationRefreshTime := time.Now().Add(1180 * time.Hour)
			accessTk := &Token{
				Username:       ggInfo.Email,
				StandardClaims: jwt.StandardClaims{ExpiresAt: expirationAccessTime.Unix()},
			}
			newRefreshTk := &Token{
				Username:       ggInfo.Email,
				StandardClaims: jwt.StandardClaims{ExpiresAt: expirationRefreshTime.Unix()},
			}
			tokenAcc := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), accessTk)
			tokenRefresh := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), newRefreshTk)

			accessTokenString, _ := tokenAcc.SignedString([]byte(os.Getenv("jwtAccessKey")))
			refreshTokenString, _ := tokenRefresh.SignedString([]byte(os.Getenv("jwtRefreshKey")))
			if getUser.RowsAffected != 0 {
				db.Model(&user).Debug().Updates(map[string]interface{}{"refresh_token": refreshTokenString, "last_logined": time.Now()})
				var usName User
				b, _ := json.Marshal(getUser.Value)
				err = json.Unmarshal(b, &usName)
				c.Header("User-Id", usName.Id)
				userDeviceToken := models.UserDeviceToken{
					UserId:      usName.Id,
					DeviceToken: tokenDevice,
				}
				db.Debug().Where(userDeviceToken).FirstOrCreate(&userDeviceToken)
				db.LogMode(true)

			} else {
				uuidRandom := uuid.New().String()

				c.Header("User-Id", uuidRandom)
				hashedPw, _ := HashPassword("")
				//google user activated = true
				newUser := User{
					Id:       uuidRandom,
					Username: ggInfo.PhoneNumber,
					Password: hashedPw,
					//Phoe
					RefreshToken: refreshTokenString,
					Email:        ggInfo.Email,
					AvatarGoogle: ggInfo.Picture,
					DisplayName:  ggInfo.Name,
					LastLogined:  time.Now(),
					IsActivated:  true,
				}
				addUser := db.Debug().Save(&newUser)
				userDeviceToken := models.UserDeviceToken{
					UserId:      uuidRandom,
					DeviceToken: tokenDevice,
				}
				db.Debug().Where(userDeviceToken).FirstOrCreate(&userDeviceToken)

				if addUser.Error != nil {
					log.Info("err::", "can not create")
					c.Writer.WriteHeader(http.StatusInternalServerError)
				}

			}

			db.LogMode(true)
			c.Header("Access-Token", accessTokenString)
			c.Header("Refresh-Token", refreshTokenString)
		}

		err = res.Body.Close()
		if err != nil {
			return
		}
	})

	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}

func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "http://171.244.49.128")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST,GET")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}

func ComparePassword(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

// isRegister 	true: register
//				false: forget pass
func sendVerificationEmail(name string, email string, randomCode int, db *gorm.DB, isRegister bool) error {
	from := os.Getenv("gmail")
	pass := "uuijiwcohupaskza"
	to := email
	var subject, message string
	/*if isRegister{
		subject = "Mã xác nhận đăng ký sử dụng ứng dụng Dhome"
		message ="Bạn đã đăng ký sử dụng ứng dụng Dhome "
	} else {*/
	subject = "Mã xác nhận ứng dụng Dhome"
	message = "Bạn đã yêu cầu mã xác nhận ứng dụng Dhome "
	//	}

	bodyMessage := "Xin chào " + name + " \n" + message + "\n" +
		"Mã xác nhận của bạn là : " + strconv.Itoa(randomCode) + "\n" +
		"Mã xác thực có hiệu lực trong 15 phút"

	msg := "From: " + from + "\n" +
		"To: " + to + "\n" +
		"Subject: " + subject + "\n\n" + bodyMessage
	db.LogMode(true)
	err := smtp.SendMail("smtp.gmail.com:587",
		smtp.PlainAuth("", from, pass, "smtp.gmail.com"),
		from, []string{to}, []byte(msg))
	return err
}

const CODE_SUCCESS = "0"
const CODE_INVALID_INPUT = "2"
const CODE_NOT_ACTIVATED = "1"
const CODE_USER_EXISTED = "1"

type Configuration struct {
	DB_USERNAME string
	DB_PASSWORD string
	DB_PORT     string
	DB_HOST     string
	DB_NAME     string
}

func GetConfig(params ...string) Configuration {
	configuration := Configuration{}
	env := "dev"
	if len(params) > 0 {
		env = params[0]
	}
	fileName := fmt.Sprintf("./%s_config.json", env)
	gonfig.GetConf(fileName, &configuration)
	return configuration
}
