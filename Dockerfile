FROM alpine
COPY authserver .
COPY prod_config.json .
#ADD iot_srv /iot_srv
#RUN go install
RUN chmod +x ./authserver
CMD ./authserver
